package config

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"sync"
)

type mainConfig struct {
	Secret map[string]string `json:"secret"`
	Open   map[string]string `json:"open"`
}

type ConfigType string

const (
	openConfig   ConfigType = "json"
	secretConfig ConfigType = "env"
)

type confData struct {
	openCfg   map[string][]byte
	secretCfg map[string][]byte
}

var cfg *confData
var once sync.Once

func init() {
	log.Println("[configurator] Init")
	once.Do(func() {
		cfg = &confData{}
		mainCfg := getMainCfg()
		cfg.openCfg = getAppCfg(mainCfg.Open, openConfig)
		cfg.secretCfg = getAppCfg(mainCfg.Secret, secretConfig)
	})
	log.Println("[configurator] OK")
}

func getMainCfg() mainConfig {
	path := parser("/configs/conf.json")
	data, err := os.ReadFile(path)
	if err != nil {
		log.Panicf("can not read config file - %s: %s", path, err)
	}
	var mainCfg mainConfig
	err = json.Unmarshal(data, &mainCfg)
	if err != nil {
		log.Panicf("can not unmarshal config file - %s: %s", path, err)
	}
	return mainCfg
}

func getAppCfg(dataCfg map[string]string, cfgType ConfigType) map[string][]byte {
	res := make(map[string][]byte)
	var secretSep string
	if cfgType == secretConfig {
		secretSep = "."
	}
	for key, val := range dataCfg {
		path := parser(fmt.Sprintf("/configs/%s%s.%s", secretSep, val, cfgType))
		data, err := os.ReadFile(path)
		if err != nil {
			log.Panicf("can not read config file - %s: %s", path, err)
		}
		res[key] = data
	}
	return res
}
