# Core Config

## 1. Конфиги

Конфиги должны располагаться в корневой папке проекта `configs`

Главный конфиг должен называться `conf.json`.

Пример наполнения:
```json
{
  "secret": {
    "app": "app"
  },
  "open": {
    "example": "example_cfg"
  }
}
```

### 1.1 Secret конфиги
_**Secret конфиги**_ - это `env` конфиги, которые подставляются при деплое. Там обычно храним важную для 
сервиса информацию, например логопасы для подключения к бд.

Пример имени файла - `.app.env`

Пример значений в конфиге - `APP_PORT: 8082` или `APP_PORT=8082`

### 1.2 Open конфиги
_**Open конфиги**_ - это `json` конфиги, которые деплоятся из репозитория вместе с сервисом. Там можно
хранить константы или какие-либо переменные, которые нужны для логики приложения

Пример имени файла - `example_cfg.env`

### 1.3 Ключи и значения `conf.json`
Верхнеуровневые ключи `secret` и `open` означают группу конфигов. По той или иной группе будет осуществляться
поиск и парсинг.

Внутри каждой группы содержатся динамические ключи и значения, т.е. ключом будет имя, по которому
будет произведен поиск нужного конфига из storage, а значением выступает имя файла, которое нужно
будет спарсить.

Пример:

содержимое `conf.json`
```json
{
  "secret": {
    "app": "app"
  },
  "open": {
    "example": "example_cfg"
  }
}
```
В группе `secret` будет ключ `app` и по этому ключу, будет находится конфиг из файла `.app.env`

В группе `open` будет ключ `example` и по этому ключу, будет находится конфиг из файла `example_cfg.json`

## Важно
_**Все конфиги должны находиться на одном уровне в папке `configs`, без вложений в какие-либо папки.**_
Пример:
```
- configs
  |- .app.env
  |- example_cfg.json
  |- conf.json
- ...
```

## 2. Использование

### 2.1 Open конфиги

Предположим, в `example_cfg.json` лежит конфиг вида
```json
{
  "test": "asd"
}
```

Подготавливаем модель
```go
type OpenCfg struct {
	Test string `json:"test"`
}
```

Импортируем конфиг

```go
resOpen, err := core_config.GetOpenConfig[OpenCfg]("example")
if err != nil {
    fmt.Println(err)
    return
}
```

### 2.1 Secret конфиги
Предположим, в `.app.env` лежит конфиг вида
```
APP_PORT: 8082

APP_BASIC_LOGIN: admin
APP_BASIC_PASSWORD: test
APP_DEBUG: false
APP_PING_HANDLER: true
```

Подготавливаем модель
```go
type AppCfg struct {
    Port               int    `env:"APP_PORT"`
    BasicLogin         string `env:"APP_BASIC_LOGIN"`
    BasicPassword      string `env:"APP_BASIC_PASSWORD"`
    Debug              bool   `env:"APP_DEBUG"`
    PingHandler        bool   `env:"APP_PING_HANDLER"`
}
```

Импортируем конфиг

```go
resSecret, err := core_config.GetSecretConfig[AppCfg]("app")
if err != nil {
    fmt.Println(err)
    return
}
```
### Особые случаи
**1. `GetSecretConfigMap`**

Импорт конфигов
```go
resSecretMap, err := core_config.GetSecretConfigMap("app")
if err != nil {
    fmt.Println(err)
    return
}
```
В данном случае результатом будет `map[string]string`

**2. `GetSecretConfigByKey`**

Поиск значения по ключу
```go
secretPort, err := core_config.GetSecretConfigByKey("app", "APP_PORT")
if err != nil {
    fmt.Println(err)
    return
}
```
Значение возвращается в формате `string`

**3. `SetSecret`**

Подстановка секрета в холдер
```go
secret, err := core_config.SetSecret(resOpen.Test, "app")
if err != nil {
    fmt.Println(err)
    return
}
fmt.Println(secret)
```
Первым параметром подается валидное поле с холдером, например `{{TEST_SECRET}}`, вторым параметром
имя `secret` файла, т.е. `.env` файла, из которого будет доставлен секрет.

Значение возвращается в формате `string`

**4. `CheckHolderFormat`**

Проверка, является ли поле холдером
```go
if core_config.CheckHolderFormat("{{TEST_HOLDER}}"){
    fmt.Println("field is holder")
}
```
Значение возвращается в формате `bool`