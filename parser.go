package config

import (
	"errors"
	"log"
	"os"
	"path"
)

func parser(fp string) string {
	filePath, err := func() (string, error) {
		directories := []string{".", ".."}
		files := []string{fp}
		for _, d := range directories {
			for _, fl := range files {
				cfg_path := path.Join(d, fl)
				if _, err := os.Stat(cfg_path); os.IsNotExist(err) {
					log.Printf("config does not exists: %s", cfg_path)
					continue
				}
				return cfg_path, nil
			}
		}
		return "", errors.New("does not exist config")
	}()
	if err != nil {
		log.Panic(err.Error())
	}

	return filePath
}
