package config

import (
	"bytes"
	"encoding/json"
	"fmt"
	"reflect"
	"regexp"
	"strconv"
	"strings"

	"github.com/joho/godotenv"
)

const (
	secretPattern = `^\{\{\s*([A-Z_ ]+)\s*\}\}$`
)

func CheckOpenConfig(name string) bool {
	_, ok := cfg.openCfg[name]
	return ok
}

func CheckSecretConfig(name string) bool {
	_, ok := cfg.secretCfg[name]
	return ok
}

func GetOpenConfig[T interface{}](name string) (T, error) {
	data, ok := cfg.openCfg[name]
	var res T
	if !ok {
		return res, fmt.Errorf("ERROR | [core_config] config [%s] does not exist", name)
	}
	err := json.Unmarshal(data, &res)
	if err != nil {
		return res, fmt.Errorf("ERROR | [core_config] error unmarshal config [%s], detatil: %s", name, err)
	}
	return res, nil
}

func GetSecretConfigMap(name string) (map[string]string, error) {
	data, ok := cfg.secretCfg[name]
	if !ok {
		return nil, fmt.Errorf("ERROR | [core_config] config [%s] does not exist", name)
	}

	buf := bytes.NewReader(data)
	resMap, err := godotenv.Parse(buf)
	if err != nil {
		return nil, fmt.Errorf("ERROR | [core_config] error parse config [%s], detatil: %s", name, err)
	}

	return resMap, nil
}

func GetSecretConfigByKey(name, key string) (string, error) {
	data, err := GetSecretConfigMap(name)
	if err != nil {
		return "", err
	}

	res, ok := data[key]
	if !ok {
		return "", fmt.Errorf("ERROR | [core_config] key [%s] not found from [%s]", key, name)
	}

	return res, nil
}

func GetSecretConfig[T interface{}](name string) (T, error) {
	data, ok := cfg.secretCfg[name]
	var res T
	if !ok {
		return res, fmt.Errorf("ERROR | [core_config] config [%s] does not exist", name)
	}

	buf := bytes.NewReader(data)
	resMap, err := godotenv.Parse(buf)
	if err != nil {
		return res, fmt.Errorf("ERROR | [core_config] error parse config [%s], detatil: %s", name, err)
	}

	val := reflect.ValueOf(&res).Elem()
	typ := val.Type()

	for i := 0; i < val.NumField(); i++ {
		field := val.Field(i)
		fieldType := typ.Field(i)
		envTag := fieldType.Tag.Get("env")

		if envTag != "" {
			envValue, exists := resMap[envTag]
			if exists {
				if fieldType.Type.Kind() == reflect.Ptr {
					ptr := reflect.New(fieldType.Type.Elem())
					setValue(ptr.Elem(), envValue)
					field.Set(ptr)
				} else {
					setValue(field, envValue)
				}
			}
		}
	}

	return res, nil
}

func setValue(field reflect.Value, value string) {
	switch field.Kind() {
	case reflect.String:
		field.SetString(value)
	case reflect.Int:
		intValue, err := strconv.Atoi(value)
		if err == nil {
			field.SetInt(int64(intValue))
		}
	case reflect.Bool:
		boolValue, err := strconv.ParseBool(value)
		if err == nil {
			field.SetBool(boolValue)
		}
	}
}

func SetSecret(holder string, secretCfgName string) (string, error) {
	re := regexp.MustCompile(secretPattern)
	if !re.MatchString(holder) {
		return "", fmt.Errorf("ERROR | [core_config] wrong auth format secret tag [%s]", holder)
	}
	return getSecretValue(re, holder, secretCfgName)
}

func CheckHolderFormat(holder string) bool {
	re := regexp.MustCompile(secretPattern)
	return re.MatchString(holder)
}

func getSecretValue(re *regexp.Regexp, val, secretCfgName string) (string, error) {
	match := re.FindStringSubmatch(val)
	if len(match) < 2 {
		return "", fmt.Errorf("ERROR | [core_config] wrong math secret tag [%s]", val)
	}
	textKey := match[1]
	if secretCfgName == "" {
		return "", fmt.Errorf("ERROR | [core_config] secretCfgName is empty")
	}
	textKey = strings.TrimSpace(textKey)
	secretVal, err := GetSecretConfigByKey(secretCfgName, textKey)
	if err != nil {
		return "", fmt.Errorf("ERROR | [core_config] secret not found with key [%s]", textKey)
	}
	return secretVal, nil
}
